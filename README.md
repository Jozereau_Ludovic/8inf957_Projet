<h1>Projet de cours UQAC - Générateur de combat pour jeu de rôles stratégiques</h1>

<p><h2>Description du travail</h2>
Logiciel permettant de créer un bestaire pour un jeu de rôles stratégique et de générer
des combats aléatoires sur des grilles générées sur le moment. 
</p>

<p><h2>Technologies et langages utilisés</h2>
<ul><li>Qt
    <li>C++
    <li>Git
</ul></p>

<p><h2>Méthode de travail</h2>
Méthodologie Agile.
</p>

<p><h2>Rôles dans le projet</h2>
<ul><li>Chef de projet
    <li>Concepteur
    <li>Programmeur
    <li>Designer
</ul></p>

<p><h2>Participants</h2>
<ul><li>Maxence Gonfalone
    <li>Ludovic Jozereau
    <li>Edouard Nguon
</ul></p>

<p><h3>Pas d'exécutable disponible actuellement.</h3></p>


## Mode d'emploi
1. Paramétrer le chemin de sauvegarde des données via les paramètres.
2. Créer des monstres via le menu "Gérer le bestaire" :
    * Choisir ses caractéristiques ;
    * Choisir/créer ses attaques.
3. Créer une carte de combat via le menu "Créer un combat".
    * Créer un ou plusieurs ensembles de cases d'obstacles ;
    * Sélectionner les paramètres voulus ;
    * Lancer la génération.
    
## Règles de la génération
* Les monstres du combat sont à deux niveaux maximum du niveau du combat voulu.
* Il doit y avoir au moins 15 + 1/4 de la carte disponible à la fin (en terme de cases non remplies par des obstacles).

## Environnement de développement
Qt Creator 4.4.1

Kit de déploiement : Desktop Qt 5.9.2 MinGW 32 bit
